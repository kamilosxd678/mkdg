#include <gdal.h>
#include <gdal_priv.h>
#include <gdal_alg.h>
#include <iostream>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\imgproc.hpp>
#include <string>
#include <sstream>

#define OUT_WINDOW_WIDTH 2560
#define OUT_WINDOW_HEIGHT 1440

#define MAPS_FOLDER "D:/mkdg/"

#define CORINE_FILE_PATH MAPS_FOLDER "x_g100_clc12_V18_5_out2.tif"
#define LANDSAT_COLOR_FILE_PATH MAPS_FOLDER "LC81900222015111LGN00.tif"
#define LANDSAT_THERMAL_FILE_PATH MAPS_FOLDER "LC81900222015111LGN00_TIR.tif"

cv::Mat gInputColorImage = cv::imread(LANDSAT_COLOR_FILE_PATH);
cv::Mat gInputCorineImage = cv::imread(CORINE_FILE_PATH);
cv::Mat gFilteredColorImage;
cv::Mat diff;
cv::Mat layerOnClc;
int clcAndLandsatHits = 0;
int clcOnlyHits = 0;
int landsatOnlyHits = 0;

cv::Mat gMerge432;
cv::Mat gMerge321;
cv::Mat gMerge742;
cv::Mat gMerge451;
cv::Mat gMerge753;

double corineADFGeoTransform[6];
double landsatADFGeoTransform[6];

std::vector<cv::Vec3b> urbanColors;

#pragma region Utility functions
#pragma region Display
void showImageWindow(const cv::Mat& img)
{
    int rwidth, rheight;
    double rmultiplier;

    if (img.rows > img.cols)
    {
        //Longer than wider
        rheight = OUT_WINDOW_HEIGHT;
        rmultiplier = ((double)img.rows) / ((double)OUT_WINDOW_HEIGHT);
        rwidth = (int)(((double)img.cols) / rmultiplier);
    }
    else
    {
        //Wider than longer
        rwidth = OUT_WINDOW_WIDTH;
        rmultiplier = ((double)img.cols) / ((double)OUT_WINDOW_WIDTH);
        rheight = (int)(((double)img.rows) / rmultiplier);
    }

    cv::Size resizeSize(rwidth, rheight);
    //cv::Size resizeSize(img.cols / 4, img.rows / 4);
    cv::Mat resizedImg;

    cv::resize(img, resizedImg, resizeSize);

    cv::imshow("Image", resizedImg);
    cv::resizeWindow("Image", resizeSize.width, resizeSize.height);
    cv::waitKey(0);
}

void printMenu()
{
    std::cout << "Choose desired option: " << std::endl;
    std::cout << "1. Display only found area" << std::endl;
    std::cout << "2. Display difference between CLC" << std::endl;
    std::cout << "3. Display as layer on CLC" << std::endl;
    std::cout << "4. Calculate difference between CLC and Landsat" << std::endl;
    std::cout << "5. Save images" << std::endl;

    std::cout << std::endl << std::endl;

    std::cout << "0. Exit" << std::endl;
}
#pragma endregion
#pragma region Image manipulation

void mergeBands(cv::OutputArray out, int rBand, int gBand, int bBand)
{
    int bandToBand[] = { 0, 2, 3, 4, 5, 6, 0, 7 };

    std::stringstream redPath;
    std::stringstream greenPath;
    std::stringstream bluePath;
    std::stringstream outPath;

    redPath << MAPS_FOLDER "LC81900222015111LGN00.tar/LC81900222015111LGN00_B" << bandToBand[rBand] << ".TIF";
    greenPath << MAPS_FOLDER "LC81900222015111LGN00.tar/LC81900222015111LGN00_B" << bandToBand[gBand] << ".TIF";
    bluePath << MAPS_FOLDER "LC81900222015111LGN00.tar/LC81900222015111LGN00_B" << bandToBand[bBand] << ".TIF";

    outPath << "out" << rBand << gBand << bBand << ".png";

    cv::Mat red = cv::imread(redPath.str(), CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat green = cv::imread(greenPath.str(), CV_LOAD_IMAGE_GRAYSCALE);
    cv::Mat blue = cv::imread(bluePath.str(), CV_LOAD_IMAGE_GRAYSCALE);

    cv::equalizeHist(red, red);
    cv::equalizeHist(green, green);
    cv::equalizeHist(blue, blue);
    
    std::vector<cv::Mat> images(3);

    images.at(0) = blue;
    images.at(1) = green;
    images.at(2) = red;
   

    cv::merge(&images[0], images.size(), out);
}

#pragma endregion
#pragma region GeoCalculations
void pixel2coord(double col, double row, double* geotransform, double* xp, double* yp)
{
    *xp = geotransform[0] + col * geotransform[1] + row * geotransform[2];
    *yp = geotransform[3] + col * geotransform[4] + row * geotransform[5];
}

void coord2pixel(double xp, double yp, double* geotransform, double* p, double* l)
{
    *p = (xp * geotransform[5] - geotransform[0] * geotransform[5] - yp * geotransform[2] + geotransform[2] * geotransform[3]) / (geotransform[1] * geotransform[5] - geotransform[2] * geotransform[4]);
    *l = (xp * geotransform[4] - geotransform[0] * geotransform[4] - yp * geotransform[1] + geotransform[1] * geotransform[3]) / (geotransform[2] * geotransform[4] - geotransform[1] * geotransform[5]);
}
#pragma endregion
#pragma endregion

#pragma region Menu options handlers
/**
*  Menu options handlers
*/

void showFilteredImage()
{
    showImageWindow(gFilteredColorImage);

}

void showClcDiffImage()
{
    showImageWindow(diff);
}

void showLayerOnClc()
{
    showImageWindow(layerOnClc);
}

void showDiff()
{
    system("cls");
    
    float percentage = ((float)clcAndLandsatHits / ((float)clcAndLandsatHits + (float)clcOnlyHits)) * 100.0f;
    std::cout << "--- Absolute differences ---" << std::endl;
    std::cout << "Urban pixels CLC: " << clcAndLandsatHits + clcOnlyHits << std::endl;
    std::cout << "Landsat urban pixels: " << clcAndLandsatHits + landsatOnlyHits << std::endl;
    std::cout << "Hits: " << clcAndLandsatHits  << std::endl;
    std::cout << "--- Relative differences ---" << std::endl;
    std::cout << "Coverage: " << (int)percentage << "%" << std::endl;
    
    system("pause");
}

void saveImages()
{
    cv::imwrite("filtered.jpg", gFilteredColorImage);
    cv::imwrite("diff.jpg", diff);
    cv::imwrite("layerOnClc.jpg", layerOnClc);
}
#pragma endregion

void filterOutWaterFromColorImage()
{
    for (int i = 0; i < gInputColorImage.rows; ++i)
    {
        for (int j = 0; j < gInputColorImage.cols; ++j)
        {
            cv::Vec3b p = gInputColorImage.at<cv::Vec3b>(i, j);

            float r = p[2] / 255.;
            float g = p[1] / 255.;
            float b = p[0] / 255.;
            float k = (1 - std::max(std::max(r, g), b));

            float cyan = (1 - r - k) / (1 - k);
            float magneta = (1 - g - k) / (1 - k);
            float yellow = (1 - b - k) / (1 - k);

            if (cyan >= magneta && cyan >= yellow)
            {
                gInputColorImage.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 0);
            }
        }
    }
}

cv::Vec3b filterBand432(cv::Vec3b p, cv::Vec3b colorImageP)
{
    if (!(colorImageP[0] == 0 && colorImageP[1] == 0 && colorImageP[2] == 0)) {
        float r = p[2] / 255.;
        float g = p[1] / 255.;
        float b = p[0] / 255.;
        float k = (1 - std::max(std::max(r, g), b));

        float cyan = (1 - r - k) / (1 - k);
        float magneta = (1 - g - k) / (1 - k);
        float yellow = (1 - b - k) / (1 - k);

        if (cyan >= magneta && cyan >= yellow)
        {
            if (g / b <= 1.1f && g / b >= 0.9f && r / b >= 0.45f && r / b <= 0.65f) {
                return cv::Vec3b(255, 255, 255);
            }
        }
    }
    return cv::Vec3b(0, 0, 0);
}

cv::Vec3b filterBand321(cv::Vec3b p, cv::Vec3b colorImageP)
{
    if (!(colorImageP[0] == 0 && colorImageP[1] == 0 && colorImageP[2] == 0)) {
        if (p[0] >= 210 && p[1] >= 210 && p[2] >= 210)
        {
            return cv::Vec3b(255, 255, 255);
        }
    }
    return cv::Vec3b(0, 0, 0);
}

cv::Vec3b filterBand742(cv::Vec3b p, cv::Vec3b colorImageP)
{
    if (!(colorImageP[0] == 0 && colorImageP[1] == 0 && colorImageP[2] == 0)) {
        float r = p[2] / 255.;
        float g = p[1] / 255.;
        float b = p[0] / 255.;
        float k = (1 - std::max(std::max(r, g), b));

        float cyan = (1 - r - k) / (1 - k);
        float magneta = (1 - g - k) / (1 - k);
        float yellow = (1 - b - k) / (1 - k);

        if (magneta >= cyan && magneta >= yellow)
        {
            if (g / b <= 0.7f && g / b >= 0.40f && r / b >= 0.7f && r / b <= 1.05f) {
                return cv::Vec3b(255, 255, 255);
            }
        }
    }
    return cv::Vec3b(0, 0, 0);
}

cv::Vec3b filterBand451(cv::Vec3b p, cv::Vec3b colorImageP)
{
    if (!(colorImageP[0] == 0 && colorImageP[1] == 0 && colorImageP[2] == 0)) {
        float r = p[2] / 255.;
        float g = p[1] / 255.;
        float b = p[0] / 255.;
        float k = (1 - std::max(std::max(r, g), b));

        float cyan = (1 - r - k) / (1 - k);
        float magneta = (1 - g - k) / (1 - k);
        float yellow = (1 - b - k) / (1 - k);

        if (cyan >= magneta && cyan >= yellow)
        {
            if (g / b <= 0.65f && g / b >= 0.4f && r / b >= 0.4f && r / b <= 0.65f) {
                return cv::Vec3b(255, 255, 255);
            }
        }
    }
    return cv::Vec3b(0, 0, 0);
}

cv::Vec3b filterBand753(cv::Vec3b p, cv::Vec3b colorImageP)
{
    if (!(colorImageP[0] == 0 && colorImageP[1] == 0 && colorImageP[2] == 0)) {
        float r = p[2] / 255.;
        float g = p[1] / 255.;
        float b = p[0] / 255.;
        float k = (1 - std::max(std::max(r, g), b));

        float cyan = (1 - r - k) / (1 - k);
        float magneta = (1 - g - k) / (1 - k);
        float yellow = (1 - b - k) / (1 - k);

        if (magneta >= cyan && magneta >= yellow)
        {
            if (g / b <= 0.7f && g / b >= 0.40f && r / b >= 0.7f && r / b <= 1.05f) {
                return cv::Vec3b(255, 255, 255);
            }
        }
    }

    return cv::Vec3b(0, 0, 0);
}

bool isPixelWhite(cv::Vec3b p)
{
    return p[0] == 255 && p[1] == 255 && p[2] == 255;
}

bool isCorinePixelUrban(cv::Vec3b p)
{
    //todo map structure would be better...
    for(cv::Vec3b& uP : urbanColors)
    {
        if(p == uP)
        {
            return true;
        }
    }
    return false;
}

cv::Vec3b getCorinePixel(int x, int y)
{
    double xp, yp;
    pixel2coord(y, x, landsatADFGeoTransform, &xp, &yp);

    double l, p;//pixels in corine
    coord2pixel(xp, yp, corineADFGeoTransform, &p, &l);

    return gInputCorineImage.at<cv::Vec3b>(p, l);
}
void findUrbanAreas()
{
    filterOutWaterFromColorImage();
    mergeBands(gMerge321, 3, 2, 1);
    mergeBands(gMerge432, 4, 3, 2);
    mergeBands(gMerge451, 4, 5, 1);
    mergeBands(gMerge742, 7, 4, 2);
    mergeBands(gMerge753, 7, 5, 3);

    std::vector<cv::Vec3b> filteredPixel;

    gFilteredColorImage = cv::Mat(gInputColorImage.rows, gInputColorImage.cols, CV_8UC3, cv::Scalar(0, 0, 0));
    diff = cv::Mat(gFilteredColorImage.rows, gFilteredColorImage.cols, CV_8UC3, cv::Scalar(0, 0, 0));
    layerOnClc = cv::Mat(gFilteredColorImage.rows, gFilteredColorImage.cols, CV_8UC3, cv::Scalar(0, 0, 0));
    for (int i = 0; i < gInputColorImage.rows; ++i)
    {
        for (int j = 0; j < gInputColorImage.cols; ++j)
        {
            cv::Vec3b colorImgPixel = gInputColorImage.at<cv::Vec3b>(i, j);
            cv::Vec3b p321 = gMerge321.at<cv::Vec3b>(i, j);
            cv::Vec3b p432 = gMerge432.at<cv::Vec3b>(i, j);
            cv::Vec3b p451 = gMerge451.at<cv::Vec3b>(i, j);
            cv::Vec3b p742 = gMerge742.at<cv::Vec3b>(i, j);
            cv::Vec3b p753 = gMerge753.at<cv::Vec3b>(i, j);

            filteredPixel.push_back(filterBand321(p321, colorImgPixel));
            filteredPixel.push_back(filterBand432(p432, colorImgPixel));
            filteredPixel.push_back(filterBand451(p451, colorImgPixel));
            filteredPixel.push_back(filterBand742(p742, colorImgPixel));
            filteredPixel.push_back(filterBand753(p753, colorImgPixel));

            int urbanHits = 0;
            for (cv::Vec3b &p : filteredPixel)
            {
                if (isPixelWhite(p))
                {
                    ++urbanHits;
                }
            }

            bool isCorineUrban = isCorinePixelUrban(getCorinePixel(j, i));
            
            if (urbanHits >= 4)
            {
                gFilteredColorImage.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 255, 255);
                layerOnClc.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 255);

                if(isCorineUrban)
                {
                    ++clcAndLandsatHits;
                    //red
                    diff.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 0, 255);
                    
                }else
                {
                    ++landsatOnlyHits;
                    //blue
                    diff.at<cv::Vec3b>(i, j) = cv::Vec3b(255, 0, 0);        
                }
            }else
            {
                if(isCorineUrban)
                {
                    ++clcOnlyHits;
                    //green
                    diff.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 255, 0);
                    layerOnClc.at<cv::Vec3b>(i, j) = cv::Vec3b(0, 255, 255);
                }

            }
            filteredPixel.clear();
        }
    }
}

int main()
{
    bool operating = true;
    int menuOption;

#pragma region GDAL Part
    GDALDataset *pCorineDataset, *pLandsatDataset;

    GDALAllRegister();

    pCorineDataset = (GDALDataset*)GDALOpen(CORINE_FILE_PATH, GA_ReadOnly);
    pLandsatDataset = (GDALDataset*)GDALOpen(LANDSAT_COLOR_FILE_PATH, GA_ReadOnly);

    pCorineDataset->GetGeoTransform(corineADFGeoTransform);
    pLandsatDataset->GetGeoTransform(landsatADFGeoTransform);

    urbanColors.push_back(cv::Vec3b(077, 000, 230));
    urbanColors.push_back(cv::Vec3b(000, 000, 255));
    urbanColors.push_back(cv::Vec3b(242, 077, 204));
    urbanColors.push_back(cv::Vec3b(000, 000, 204));
    urbanColors.push_back(cv::Vec3b(204, 204, 230));
    urbanColors.push_back(cv::Vec3b(230, 204, 230));
    urbanColors.push_back(cv::Vec3b(204, 000, 166));
    urbanColors.push_back(cv::Vec3b(000, 077, 166));
    urbanColors.push_back(cv::Vec3b(255, 077, 255));
    urbanColors.push_back(cv::Vec3b(255, 166, 255));
    urbanColors.push_back(cv::Vec3b(255, 230, 255));
#pragma endregion
    findUrbanAreas();
#pragma region Main menu loop
    while (operating)
    {
        system("cls");
        printMenu();
        std::cin >> menuOption;

        switch (menuOption)
        {
        case 0:
            operating = false;
            break;

        case 1:
            //found area
            showFilteredImage();
            break;

        case 2:
            //clc diff
            showClcDiffImage();
            break;


        case 3:
            //as layer on clc
            showLayerOnClc();
            break;

        case 4:
            //Show difference
            showDiff();
            break;

        case 5:
            //Show difference
            saveImages();
            break;
        }
    }
#pragma endregion

    return 0;
}